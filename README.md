# About #
This script scans the vobs that are using 95% of disk and automatically executes scrubber to perform housekeeping.

# Usage #
```
#!bash
./vob95.ksh
```
