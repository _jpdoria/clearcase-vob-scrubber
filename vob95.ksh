#!/usr/bin/ksh
# Script:       VOB Scrubber (vob95.sh)
# Author:       John Paul P. Doria
# Date Created: July 16, 2013

logdate=`date +%m%d%y%H%M%S`
log=/home/$USER/vobscrub_${logdate}.log

trap "echo \"Interrupted.\";rm -f $log > /dev/null 2>&1" 1 2 3 15

getList() {
	df -h | sort | grep ccvobs | awk {'print $6'}
}

threshold() {
	df -h $vob | awk {'getline;print $5'} | sed 's/%//g'
}

scan() {
	echo "Scanning filesystem..."
	sleep 3
	for vob in `getList`; do
		if [ `threshold` -ge 95 ]; then
			echo "Warning: $vob is at `threshold`%."
			echo "Warning: $vob is at `threshold`%." >> $log
		fi
	done
}

scrub() {
	echo "Initializing VOB scrubber..."
	sleep 3
	for vob in `getList`; do
		if [ `threshold` -ge 95 ]; then
			echo "Cleaning $vob..."
			echo "Cleaning $vob..." >> $log
			/usr/atria/etc/scrubber -e -k cltxt $vob
			sleep 1
		fi
	done
	echo "Done!"
	echo "Done!" >> $log
	echo "########################################################" >> $log
	echo "# End time: `date`" >> $log
	echo "########################################################" >> $log
	echo "Log file can be found at: $log."
}

main() {
	echo "########################################################" >> $log
	echo "# Start time: `date`" >> $log
	echo "########################################################" >> $log
	scan && scrub
	exit 0
}

main
